import React, { FC, useState } from 'react';
import { View, TextInput, StyleSheet, PermissionsAndroid } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Contact } from 'expo-contacts';
import * as Contacts from 'expo-contacts';
import { StackScreenProps } from '@react-navigation/stack';
import { StackParamList } from '../App';

type Props = StackScreenProps<StackParamList, 'CreateContact'>;

const CreateContact: FC<Props> = ({ navigation }: Props) => {
  const [contactValue, setContactValue] = useState<Contact>({
    id: '',
    firstName: '',
    lastName: '',
    contactType: 'person',
    name: '',
    phoneNumbers: [
      {
        id: '',
        label: '',
        number: '',
      },
    ],
  });

  const handleAddContact = async (): Promise<void> => {
    const permission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS
    );

    if (permission === 'granted') {
      await Contacts.addContactAsync(contactValue);
      navigation.goBack();
    }
  };

  return (
    <View>
      <View style={styles.avatar}>
        <TouchableOpacity>
          <Ionicons
            style={styles.avatarIcon}
            name="camera-reverse-outline"
            size={150}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.body}>
        <TextInput
          style={styles.input}
          placeholder="firstname"
          maxLength={40}
          value={contactValue.firstName}
          onChangeText={(text) =>
            setContactValue({
              ...contactValue,
              firstName: text,
            })
          }
        />
        <TextInput
          style={styles.input}
          placeholder="lastname"
          maxLength={40}
          onChangeText={(text) =>
            setContactValue({
              ...contactValue,
              lastName: text,
            })
          }
        />
        <TextInput
          style={styles.input}
          placeholder="mobile number"
          keyboardType="numeric"
          onChangeText={(text) =>
            setContactValue({
              ...contactValue,
              phoneNumbers: [
                {
                  id: '1',
                  label: 'mobile',
                  number: text,
                },
              ],
            })
          }
        />
      </View>
      <View style={styles.button}>
        <TouchableOpacity>
          <Ionicons
            style={styles.checkIcon}
            name="checkmark-outline"
            size={50}
            color="blue"
            onPress={handleAddContact}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  avatar: {
    backgroundColor: 'white',
    height: 250,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30,
  },
  avatarIcon: {
    color: 'rgb(242, 242, 242)',
  },
  body: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  input: {
    fontSize: 20,
    marginBottom: 20,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'blue',
    borderStyle: 'solid',
  },
  button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  checkIcon: {
    backgroundColor: 'white',
    borderRadius: 50,
    fontWeight: 'bold',
    padding: 10,
  },
});

export default CreateContact;
