import React, { FC } from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Linking,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Ionicons } from '@expo/vector-icons';
import { Contact } from 'expo-contacts';
import * as Contacts from 'expo-contacts';
import { StackScreenProps } from '@react-navigation/stack';
import { StackParamList } from '../App';

type Props = StackScreenProps<StackParamList, 'ContactProfile'>;

const ContactProfile: FC<Props> = ({ navigation, route }: Props) => {
  const {
    id,
    firstName,
    lastName,
    imageAvailable,
    image,
    phoneNumbers,
    name,
  }: Contact = route.params.contact;

  const handleCall = (): void => {
    if (phoneNumbers?.length) {
      Linking.openURL(`tel:${phoneNumbers[0].number}`);
    }
  };

  const confirmDelete = async (): Promise<void> => {
    const permission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS
    );

    if (permission === 'granted') {
      await Contacts.removeContactAsync(id);
      navigation.goBack();
    }
  };

  const handleDeleteContact = (): void => {
    Alert.alert(
      'Confirm',
      `Are you sure you want to delete the contact ${name}`,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        { text: 'OK', onPress: () => confirmDelete() },
      ]
    );
  };

  return (
    <View style={styles.root}>
      <View style={styles.avatar}>
        {imageAvailable && <Image source={{ uri: image as string }} />}
      </View>
      <View style={styles.info}>
        <Text style={styles.label}>firstname</Text>
        <Text style={styles.text}>{firstName}</Text>
        <Text style={styles.label}>lastname</Text>
        <Text style={styles.text}>{lastName}</Text>
        {phoneNumbers?.map(({ id, label, number }) => (
          <View key={id}>
            <Text style={styles.label}>{label}</Text>
            <Text style={styles.text}>{number}</Text>
          </View>
        ))}
      </View>
      <View style={styles.buttons}>
        <TouchableOpacity>
          <Ionicons
            style={styles.icon}
            name="call-outline"
            size={50}
            color="green"
            onPress={handleCall}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Ionicons
            style={styles.icon}
            name="trash-outline"
            size={50}
            color="red"
            onPress={handleDeleteContact}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    display: 'flex',
  },
  avatar: {
    height: 250,
    backgroundColor: 'white',
  },
  info: {
    padding: 15,
    marginTop: 30,
  },
  label: {
    fontSize: 18,
  },
  text: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 5,
    minHeight: 30,
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingLeft: 50,
    paddingRight: 50,
  },
  icon: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 50,
  },
});

export default ContactProfile;
