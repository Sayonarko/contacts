import React, { FC, useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Button } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import ContactItem from '../components/ContactItem';
import * as Contacts from 'expo-contacts';
import { Contact } from 'expo-contacts';
import { useIsFocused } from '@react-navigation/core';
import { Navigation } from '../enums';
import { StackScreenProps } from '@react-navigation/stack';
import { StackParamList } from '../App';

type Props = StackScreenProps<StackParamList, 'MyContacts'>;

const MyContacts: FC<Props> = ({ navigation }: Props) => {
  const isFocused = useIsFocused();
  const [contacts, setContacts] = useState<Contact[]>([]);

  useEffect(() => {
    getAllContacts();
  }, [isFocused]);

  const getAllContacts = async (): Promise<void> => {
    const { status } = await Contacts.requestPermissionsAsync();
    if (status === 'granted') {
      const { data } = await Contacts.getContactsAsync();
      setContacts(data);
    }
  };

  const handleSearchContact = (text: string): void => {
    if (text) {
      const regex = new RegExp(text, 'i');
      const filterSearch = ({ firstName, lastName }: Contact): boolean =>
        regex.test(firstName as string) || regex.test(lastName as string);

      setContacts(contacts.filter(filterSearch));
    } else {
      getAllContacts();
    }
  };

  return (
    <View style={styles.root}>
      <Button
        title="Create contact +"
        onPress={() => navigation.navigate(Navigation.createContact)}
      />
      <View style={styles.search}>
        <TextInput
          style={styles.searchInput}
          placeholder="search contact"
          onFocus={() => handleSearchContact('')}
          onChangeText={handleSearchContact}
        />
      </View>
      <FlatList
        style={styles.list}
        data={contacts}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate(Navigation.contactProfile, { contact: item })
            }
          >
            <ContactItem contact={item} />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    position: 'relative',
    flexGrow: 1,
  },
  search: {
    padding: 15,
  },
  searchInput: {
    backgroundColor: 'white',
    fontSize: 18,
    padding: 10,
    borderRadius: 10,
  },
  list: {
    paddingLeft: 30,
  },
});

export default MyContacts;
