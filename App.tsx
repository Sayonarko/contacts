import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { getTitleProfile } from './helpers';
import { Navigation } from './enums';
import { Contact } from 'expo-contacts';

import { ContactProfile, CreateContact, MyContacts } from './views';

export type StackParamList = {
  MyContacts: undefined;
  ContactProfile: { contact: Contact };
  CreateContact: undefined;
};

const Stack = createStackNavigator<StackParamList>();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={Navigation.myContacts}>
        <Stack.Screen
          name={Navigation.myContacts}
          component={MyContacts}
          options={{ title: 'My Contacts' }}
        />
        <Stack.Screen
          name={Navigation.contactProfile}
          component={ContactProfile}
          options={getTitleProfile}
        />
        <Stack.Screen
          name={Navigation.createContact}
          component={CreateContact}
          options={{ title: 'Create contact' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
