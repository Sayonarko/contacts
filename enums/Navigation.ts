export enum Navigation {
  contactProfile = 'ContactProfile',
  myContacts = 'MyContacts',
  createContact = 'CreateContact',
}
