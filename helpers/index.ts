export const getTitleProfile = ({ route }: any) => {
  const { name } = route?.params?.contact;
  const title = `${name}`.toUpperCase();

  return { title };
};
