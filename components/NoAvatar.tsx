import React, { FC } from 'react';
import { View, Text, StyleSheet } from 'react-native';

interface INoAvatarProps {
  firstName: string | undefined;
}

const NoAvatar: FC<INoAvatarProps> = ({ firstName }: INoAvatarProps) => (
  <View style={styles.root}>
    <Text style={styles.text}>{firstName ? firstName[0] : null}</Text>
  </View>
);

const styles = StyleSheet.create({
  root: {
    width: 50,
    height: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: 'blue',
  },
  text: {
    color: 'white',
    fontFamily: 'sans-serif',
    fontWeight: 'bold',
    fontSize: 22,
  },
});

export default NoAvatar;
