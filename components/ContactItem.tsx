import React, { FC } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Contact } from 'expo-contacts';

import NoAvatar from './NoAvatar';

interface IContactItemProps {
  contact: Contact;
}

const ContactItem: FC<IContactItemProps> = ({
  contact: { imageAvailable, image, firstName, lastName },
}: IContactItemProps) => (
  <View style={styles.root}>
    {imageAvailable ? (
      <View style={styles.avatar}>
        <Image style={styles.image} source={{ uri: image as string }} />
      </View>
    ) : (
      <NoAvatar firstName={firstName} />
    )}
    <View style={styles.names}>
      <Text style={styles.name}>{firstName}</Text>
      <Text style={styles.name}>{lastName}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  root: {
    display: 'flex',
    flexDirection: 'row',
    padding: 15,
  },
  avatar: {
    width: 50,
    height: 50,
    overflow: 'hidden',
    borderRadius: 50,
  },
  image: {
    height: '100%',
  },
  names: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: 18,
  },
  name: {
    fontSize: 22,
    marginLeft: 15,
  },
});

export default ContactItem;
